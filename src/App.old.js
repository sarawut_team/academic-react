import React, { Component } from 'react';
//import logo from './logo.svg';
import logo from './LogoIndex.png';
import './App.css';
import web3 from './web3';
import academic from './academic';

class App extends Component {

  state = {
    students: [],
    balance: '',
    account: ''
  }

  async componentDidMount() {
    const students = await academic.methods.countStudents().call();
    const balance = await web3.eth.getBalance(academic.options.address);

    this.setState({students, balance});
  }

  onClick = async () => {
    const accounts = await academic.methods.setStudent(web3.eth.defaultAccount, "ID", "Sarawut", "IT", "DN", "A", "B", "C", "D").send({
      from: "0xB77F2952F54E26414780f6B05a30633559295AC4"
    })
    
    this.setState({accounts});

  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Academic Smart Contract Grade Report</h1>
        </header>
        <p className="App-intro">
          This site create to complete Master Project and using Blockchain technology.
        </p>
        <p>
          Currently grade assigned to the student are ( {this.state.students} ) peoples. Admin balance left {web3.utils.fromWei(this.state.balance, 'ether')} Ether!
        </p>
        <hr />
        <h4>Please input Student infomation and grade</h4>
          <img id="loader" src="https://loading.io/spinners/double-ring/lg.double-ring-spinner.gif" />

          <label for="id" className="col-lg-2 control-label">ID</label>
          <input id="id" type="text" />

          <label for="fullName" className="col-lg-2 control-label">First Name - Last Name</label>
          <input id="fullName" type="text" />

          <label for="department" className="col-lg-2 control-label">Department</label>
          <input id="department" type="text" />

          <label for="faculty" className="col-lg-2 control-label">Faculty</label>
          <input id="faculty" type="text" />
        <hr />
        <h4>Please input Student infomation and grade</h4>
          <label for="subject1" className="col-lg-2 control-label">Subject Name _1 (please input grade)</label>
          <input id="subject1" type="text" />

          <label for="subject2" className="col-lg-2 control-label">Subject Name _1 (please input grade)</label>
          <input id="subject2" type="text" />

          <label for="subject3" className="col-lg-2 control-label">Subject Name _1 (please input grade)</label>
          <input id="subject3" type="text" />

          <label for="subject4" className="col-lg-2 control-label">Subject Name _1 (please input grade)</label>
          <input id="subject4" type="text" />

          <button onClick={this.onClick}> Submit Grade</button>
      </div>
    );
  }
}

export default App;
