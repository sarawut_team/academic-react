import React, { Component } from 'react';
import logo from './LogoIndex.png';
import './App.css';
import web3 from './web3';
import academic from './academic';

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      stdId: '',
      fullName: '',
      department: '',
      faculty: '',
      subject1: '',
      subject2: '',
      subject3: '',
      subject4: '',
      getStudentInfo: '',
      students: [],
      balance: '',
      account: '',
      studentInfo: [],
      getStudentGrade: [],
      getStdAllAddr: []
    }
  }

  handleGetStdId = (e) => {
    this.setState({ stdId: e.target.value });
  }

  handleGetFullName = (e) => {
    this.setState({ fullName: e.target.value });
  }

  handleGetDepartment = (e) => {
    this.setState({ department: e.target.value });
  }

  handleGetFaculty = (e) => {
    this.setState({ faculty: e.target.value });
  }

  handleGetSubject1 = (e) => {
    this.setState({ subject1: e.target.value });
  }

  handleGetSubject2 = (e) => {
    this.setState({ subject2: e.target.value });
  }

  handleGetSubject3 = (e) => {
    this.setState({ subject3: e.target.value });
  }

  handleGetSubject4 = (e) => {
    this.setState({ subject4: e.target.value });
  }

  handlegetStudentInfo = (e) => {
    this.setState({getStudentInfo: e.target.value})
  }

  submit = () => {
    console.log(this.state)
  }

  async componentDidMount() {
    const students = await academic.methods.countStudents().call();
    //const balance = await web3.eth.getBalance(academic.options.address);
    //const studentInfo = await academic.methods.getStudentInfo().call();

    this.setState({students});
  }

  onClick = async () => {
    const accounts = await web3.eth.getAccounts()
    console.log('accounts: ', accounts[1]);

    // const accCreate = await web3.eth.accounts.create([123456]);
    // console.log(accCreate);
    // console.log(walletcreate);
    // const setStudent = await academic.methods.setStudent(accounts[0], this.state.stdId, this.state.fullName, this.state.department,this.state.faculty, this.state.subject1, this.state.subject2, this.state.subject3, this.state.subject4).send({
    //   from: accounts[0]
    // })
    // console.log("account[0]+++++++++++" + accounts);
    // console.log("aweb3.eth.accounts[1]" + web3.eth.accounts[1]);
    // console.log('web3.eth.accounts[0] ', web3.eth.accounts[0]);
    // console.log(setStudent);
    // this.setState({setStudent});

  }

  onClickInfo = async () => {
    // const address = await web3.eth.address;
    const address = '0xB77F2952F54E26414780f6B05a30633559295AC4';
    console.log('address: ', address);
    const getStudentInfo = await academic.methods.getStudentInfo(address).call({}, function(error, result) {
      console.log(result.id)
    })

    console.log(getStudentInfo);
    console.log('ID: ', getStudentInfo[0]);
    console.log('Fullname: ', getStudentInfo[1]);
    console.log('Deparment: ', getStudentInfo[2]);
    console.log('Faculty: ', getStudentInfo[3]);
    
    const studentInfoText = 'ID: ' + getStudentInfo[0] + ', Fullname: ' + getStudentInfo[1] + ', Faculty: ' + getStudentInfo[2] + ', Deparment :' + getStudentInfo[3];
    this.setState({getStudentInfo: studentInfoText}) 
  }

  onClickGrade = async () => {
    const address = "0xF8870D72afE05A99E455FB094aEe66084333db6F";
    const getStudentGrade = await academic.methods.getStudentGrade(address).call({}, function(error, result) {
      console.log(result.id)
    })

    console.log(getStudentGrade);
    console.log('Subject_01: ', getStudentGrade[0]);
    console.log('Subject_02: ', getStudentGrade[1]);
    console.log('Subject_03: ', getStudentGrade[2]);
    console.log('Subject_04: ', getStudentGrade[3]);
    
    const studentGradeText = 'Subject_01: ' + getStudentGrade[0] + ', Subject_02: ' + getStudentGrade[1] + ', Subject_03: ' + getStudentGrade[2] + ', Subject_04 :' + getStudentGrade[3];
    this.setState({getStudentGrade: studentGradeText}) 
  }

  onClickGetAllAddr = async () => {
    // const address = await web3.eth.address;
    const getStdAllAddr = await academic.methods.getStudents().call({from: "0xB77F2952F54E26414780f6B05a30633559295AC4"}, function(error, result) {
      console.log("this is error " + error);
      console.log("this is result " + result);
    });
    // console.log(accounts);
    this.setState({getStdAllAddr});
    console.log(typeof(getStdAllAddr));
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to Academic Smart Contract Grade Report</h1>
        </header>
        <h1>Academic Grade Report</h1>
        <p className="App-intro">
          This site create to complete Master Project and using Blockchain technology.
        </p>
        <p>
          Currently grade assigned to the student are ( {this.state.students} ) peoples.
        </p>
        <hr />
        <h4>Please input Student infomation and grade</h4>

        <img id="loader" src="https://loading.io/spinners/double-ring/lg.double-ring-spinner.gif" />

        <label for="id" class="col-lg-2 control-label">ID</label>
        <input id="id" onChange={this.handleGetStdId} type="text" />

        <label for="fullName" class="col-lg-2 control-label">First Name - Last Name</label>
        <input id="fullName" onChange={this.handleGetFullName} type="text" />

        <label for="department" class="col-lg-2 control-label">Department</label>
        <input id="department" onChange={this.handleGetDepartment} type="text" />

        <label for="faculty" class="col-lg-2 control-label">Faculty</label>
        <input id="faculty" onChange={this.handleGetFaculty} type="text" />

        <h2 id="gradeTrans"></h2>
        <span id="gradeTrans"></span>

        <label for="subject1" class="col-lg-2 control-label">Subject Name _1 (please input grade)</label>
        <input id="subject1" onChange={this.handleGetSubject1} type="text" />

        <label for="subject2" class="col-lg-2 control-label">Subject Name _2 (please input grade)</label>
        <input id="subject2" onChange={this.handleGetSubject2} type="text" />

        <label for="subject3" class="col-lg-2 control-label">Subject Name _3 (please input grade)</label>
        <input id="subject3" onChange={this.handleGetSubject3} type="text" />

        <label for="subject4" class="col-lg-2 control-label">Subject Name _4 (please input grade)</label>
        <input id="subject4" onChange={this.handleGetSubject4} type="text" />

        <button id="button" onClick={this.onClick}>Submit Grade</button>

        <hr />
        <label for="getStudentInfo" class="col-lg-2 control-label">Please input student address</label>
        <input id="getStudentInfo" onChange={this.handlegetStudentInfo} type="text" />
        <button id="button" onClick={this.onClickInfo}>Get Student Info</button>
        <p>
          Student infomation {this.state.getStudentInfo}
        </p>
        <hr />
        <label for="getStudentGrade" class="col-lg-2 control-label">Please input student address</label>
        <input id="getStudentGrade" onChange={this.handlegetStudentGrade} type="text" />
        <button id="button" onClick={this.onClickGrade}>Get Student Grade</button>
        <p>
          Student Grade {this.state.getStudentGrade}
        </p>
        <hr />
        <button id="button" onClick={this.onClickGetAllAddr}>Get Student address</button>
        <p>
          All Address {this.state.getStdAllAddr}
        </p>

      </div>

    );
  }
}

export default App;
