import React, { Component } from 'react';

import academic from '../academic';
import '../App.css';

class Grade extends Component {
  constructor(props){
    super(props)
    this.state = {
      stdInfoById: [],
      stdGradeById: [],
      stdInfoByWallet: [],
      stdGradeByWallet: [],
      getStudentId: '',
      getStudentWallet: '',
      gpa: 0,
      successById: false,
      successByWallet: false
    }
  }

  handlegetStudentId = (e) => {
    this.setState({getStudentId: e.target.value})
  }

  handlegetStudentWallet = (e) => {
    this.setState({getStudentWallet: e.target.value})
  }

  handleGPA = (grade) => {
    const subjects = grade.length
    const weight = subjects*3
    let sumGrade = 0
    let gpa = 0
    grade.forEach((g, index) => {
      switch (g.grade) {
        case 'A':
          sumGrade += 4*3
          break;
        case 'B+':
          sumGrade += 3.5*3
          break;
        case 'B':
          sumGrade += 3*3
          break;
        case 'C+':
          sumGrade += 2.5*3
          break;
        case 'C':
          sumGrade += 2*3
          break;
        case 'D+':
          sumGrade += 1.5*3
          break;
        case 'D':
          sumGrade += 1*3
          break;
        case 'F':
          sumGrade += 0*3
          break;
        default:
          break;
      }
    })
    gpa = sumGrade/weight
    this.setState({gpa: gpa.toFixed(3)})
  }

  onClickById = async () => {
    // const id = "5907031857034";
    const id = this.state.getStudentId;
    const getStudentGrade = await academic.methods.getStudentGrade(id).call({}, function(error, result) {
      console.log(result.id)
    })

    const getStudentInfo = await academic.methods.getStudentInfo(id).call({}, function(error, result) {
      console.log(result.id)
    })

    console.log(getStudentInfo);
    console.log('ID: ', getStudentInfo[0]);
    console.log('Wallet: ', getStudentInfo[1]);
    console.log('Fullname: ', getStudentInfo[2]);
    console.log('Deparment: ', getStudentInfo[3]);
    console.log('Faculty: ', getStudentInfo[4]);
    
    this.setState({stdInfoById: [getStudentInfo[0],getStudentInfo[1],getStudentInfo[2],getStudentInfo[3],getStudentInfo[4]]}) 
 
    console.log(getStudentGrade);
    console.log('Semester_01: ', getStudentGrade[0]);
    console.log('Semester_02: ', getStudentGrade[1]);
    console.log('Semester_03: ', getStudentGrade[2]);
    console.log('Semester_04: ', getStudentGrade[3]);
    
    getStudentGrade[0] = JSON.parse(getStudentGrade[0])
    getStudentGrade[1] = JSON.parse(getStudentGrade[1])
    getStudentGrade[2] = JSON.parse(getStudentGrade[2])
    getStudentGrade[3] = JSON.parse(getStudentGrade[3])
    
    let stdGradeById = []
    for(let i = 0; i<getStudentGrade[0].length; i++){
      stdGradeById.push(getStudentGrade[0][i])
    }
    for(let i = 0; i<getStudentGrade[1].length; i++){
      stdGradeById.push(getStudentGrade[1][i])
    }
    for(let i = 0; i<getStudentGrade[2].length; i++){
      stdGradeById.push(getStudentGrade[2][i])
    }
    for(let i = 0; i<getStudentGrade[3].length; i++){
      stdGradeById.push(getStudentGrade[3][i])
    }
    this.setState({stdGradeById: stdGradeById, successById: true}, () => this.handleGPA(this.state.stdGradeById))
  }

  onClickByWallet = async () => {
    // const wallet = '0xD9c9D9869C26f8c2DF643ac5A4b11Ac6e87d1aeb';
    const wallet = this.state.getStudentWallet;
    console.log('address: ', wallet);
    const getStdGradebyWallet = await academic.methods.getSTDGradebyWallet(wallet).call({}, function(error, result) {
      console.log(result.id)
    })
    const getStdInfobyWallet = await academic.methods.getSTDinfobyWallet(wallet).call({}, function(error, result) {
      console.log(result.id)
    })

    console.log(getStdInfobyWallet);
    console.log('ID: ', getStdInfobyWallet[0]);
    console.log('Wallet: ', getStdInfobyWallet[1]);
    console.log('Fullname: ', getStdInfobyWallet[2]);
    console.log('Deparment: ', getStdInfobyWallet[3]);
    console.log('Faculty: ', getStdInfobyWallet[4]);
    
    this.setState({stdInfoByWallet: [getStdInfobyWallet[0],getStdInfobyWallet[1],getStdInfobyWallet[2],getStdInfobyWallet[3],getStdInfobyWallet[4]]}) 

    console.log(getStdGradebyWallet);
    console.log('Semester_01: ', getStdGradebyWallet[0]);
    console.log('Semester_02: ', getStdGradebyWallet[1]);
    console.log('Semester_03: ', getStdGradebyWallet[2]);
    console.log('Semester_04: ', getStdGradebyWallet[3]);
    getStdGradebyWallet[0] = JSON.parse(getStdGradebyWallet[0])
    getStdGradebyWallet[1] = JSON.parse(getStdGradebyWallet[1])
    getStdGradebyWallet[2] = JSON.parse(getStdGradebyWallet[2])
    getStdGradebyWallet[3] = JSON.parse(getStdGradebyWallet[3])
    
    let stdGradeByWallet = []
    for(let i = 0; i<getStdGradebyWallet[0].length; i++){
      stdGradeByWallet.push(getStdGradebyWallet[0][i])
    }
    for(let i = 0; i<getStdGradebyWallet[1].length; i++){
      stdGradeByWallet.push(getStdGradebyWallet[1][i])
    }
    for(let i = 0; i<getStdGradebyWallet[2].length; i++){
      stdGradeByWallet.push(getStdGradebyWallet[2][i])
    }
    for(let i = 0; i<getStdGradebyWallet[3].length; i++){
      stdGradeByWallet.push(getStdGradebyWallet[3][i])
    }
    this.setState({stdGradeByWallet: stdGradeByWallet, successByWallet: true}, () => this.handleGPA(this.state.stdGradeByWallet))
  }

  showInfo(info) {
    const head = ['ID', 'Wallet', 'Full name', 'Faculty', 'Department']
    return info.map((info, index) => 
      <p>{head[index]}: {info}</p>
    )
  }

  showGrade(grade) {
    return grade.map((grade, index) =>
      <p>Subject_{index+1} {grade.subjectId} {grade.subjectName}: {grade.grade}</p>
    )
  }

  showGPA(){
    return <p>GPA: {this.state.gpa}</p>
  }

  render() {
    return (
      <div className="Grade">
        <label for="getStudentById" class="col-lg-2 control-label">Please input student ID</label>
        <input id="getStudentById" onChange={this.handlegetStudentId} type="text" />
        <button id="button" onClick={this.onClickById}>Get Student Info by ID</button>
        <div>
          {this.showInfo(this.state.stdInfoById)}
        </div>
        <div>
          {this.showGrade(this.state.stdGradeById)}
          {this.state.successById && this.showGPA()}
        </div>
        <hr />
        <label for="getStudentByWallet" class="col-lg-2 control-label">Please input student wallet</label>
        <input id="getStudentByWallet" onChange={this.handlegetStudentWallet} type="text" />
        
        <button id="button" onClick={this.onClickByWallet}>Get Student Info by Wallet</button>
        <div>
          {this.showInfo(this.state.stdInfoByWallet)}
        </div>
        <div>
          {this.showGrade(this.state.stdGradeByWallet)}
          {this.state.successByWallet && this.showGPA()}
        </div>
      </div>

    );
  }
}

export default Grade;
