import React, { Component } from 'react';

import web3 from '../web3';
import academic from '../academic';
import students from '../student.json';
import '../App.css';

class Admin extends Component {
  constructor(props){
    super(props)
    this.state = {
      stdId: '',
      wallet: '',
      fullName: '',
      department: '',
      faculty: '',
      subjects: [{ 
        "subjectId": "07011593", 
        "subjectName": "Data Structure and Algorithm", 
        "grade": "" 
      },
      { 
        "subjectId": "070315100", 
        "subjectName": "Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315102", 
        "subjectName": "Quantitative Analysis for Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315203", 
        "subjectName": "Telecommunication Technology", 
        "grade": "" 
      },
      { 
        "subjectId": "07011593", 
        "subjectName": "Data Structure and Algorithm", 
        "grade": "" 
      },
      { 
        "subjectId": "070315100", 
        "subjectName": "Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315102", 
        "subjectName": "Quantitative Analysis for Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315203", 
        "subjectName": "Telecommunication Technology", 
        "grade": "" 
      },
      { 
        "subjectId": "07011593", 
        "subjectName": "Data Structure and Algorithm", 
        "grade": "" 
      },
      { 
        "subjectId": "070315100", 
        "subjectName": "Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315102", 
        "subjectName": "Quantitative Analysis for Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315203", 
        "subjectName": "Telecommunication Technology", 
        "grade": "" 
      },
      { 
        "subjectId": "07011593", 
        "subjectName": "Data Structure and Algorithm", 
        "grade": "" 
      },
      { 
        "subjectId": "070315100", 
        "subjectName": "Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315102", 
        "subjectName": "Quantitative Analysis for Data Communication and Networking", 
        "grade": "" 
      },
      { 
        "subjectId": "070315203", 
        "subjectName": "Telecommunication Technology", 
        "grade": "" 
      }],
      students: [],
      balance: '',
      account: '',
      transactionHash: '',
      success: false
    }
  }

  handleGetStdId = (e) => {
    this.setState({ stdId: e.target.value });
  }

  handleGetWallet = (e) => {
    this.setState({ wallet: e.target.value});
  }

  handleGetFullName = (e) => {
    this.setState({ fullName: e.target.value });
  }

  handleGetDepartment = (e) => {
    this.setState({ department: e.target.value });
  }

  handleGetFaculty = (e) => {
    this.setState({ faculty: e.target.value });
  }

  handleGetSubjects = (e) => {
    let subjects = this.state.subjects
    const index = e.target.id
    subjects[index-1].grade = e.target.value
    this.setState({ 
      subjects: subjects
    }, ()=>{console.log(this.state.subjects[index-1])});
  }

  onChangeStdInfo = (e) => {
    const index = e.target.selectedIndex === null ? 0 : e.target.selectedIndex-1;
    console.log(index)
    if(index>-1){
      const student = students[index]
      this.setState({
        stdId: student.stdId,
        wallet: student.wallet,
        fullName: student.fullName,
        department: student.department,
        faculty: student.faculty
      }, ()=>{
        console.log(this.state)
      })
    }
  }
  async componentDidMount() {
    const students = await academic.methods.countStudents().call();

    this.setState({students});
  }

  onClick = async () => {
    this.setState({success: false})
    const accounts = await web3.eth.getAccounts()
    console.log('accounts: ', accounts[0]);
    let txHash = ''
    
    const grades = await this.checkGrades(this.state.subjects)
    console.log(grades)
    const setStudent = await academic.methods.setStudent(accounts[0], 
        this.state.stdId, 
        this.state.wallet, 
        this.state.fullName, 
        this.state.department,
        this.state.faculty, 
        ""+grades[0],
        ""+grades[1],
        ""+grades[2],
        ""+grades[3]
      ).send({from: accounts[0]}, function(error, transactionHash) {
        console.log(transactionHash)
        txHash = transactionHash
      })
      this.setState({transactionHash: txHash, success: true})
      
    console.log("account[0] = " + accounts);
    console.log('web3.eth.accounts[0] = ', web3.eth.accounts[0]);
    this.setState({setStudent});

  }

  checkGrades(subjects) {
    let semester1 = []
    let semester2 = []
    let semester3 = []
    let semester4 = []

    for(let i = 0; i<subjects.length; i++){
      if(subjects[i].grade !== ""){
        console.log(i/4)
        if(i/4 < 1){
          semester1.push(subjects[i])
        }
        else if(i/4 < 2){
          semester2.push(subjects[i])
        }
        else if(i/4 < 3){
          semester3.push(subjects[i])
        }
        else{
          semester4.push(subjects[i])
        }
      }
    }

    console.log('semester1', semester1)
    console.log('semester2', semester2)
    console.log('semester3', semester3)
    console.log('semester4', semester4)

    return [JSON.stringify(semester1), JSON.stringify(semester2), JSON.stringify(semester3), JSON.stringify(semester4)]
  }

  showStudentDropdown(){
    return students.map((student, index) => 
      <option value={student.stdId} >{student.stdId}</option>
    )
  }

  renderGradeFields(subjects){
    return subjects.map((subject, index) => 
      <div>
        <label for={index+1} class="col-lg-2 control-label"> Subject_{index+1} {subject.subjectId} {subject.subjectName} (please input grade)</label>
        <input id={index+1} onChange={this.handleGetSubjects} type="text" />
      </div>
    )
  }

  render() {
    return (
      <div className="Admin">
        <p>
          Contract currently submit total are ( {this.state.students} ) times.
        </p>
        <hr />
        <h4>Please input Student infomation and grade</h4>

        <img id="loader" src="https://loading.io/spinners/double-ring/lg.double-ring-spinner.gif" alt="loader"/>

        <label for="id" class="col-lg-2 control-label">ID</label>
        <select value={this.state.stdId} onChange={this.onChangeStdInfo}>
          <option value="" ></option>
          {this.showStudentDropdown()}
        </select>
        <p></p>

        <label for="wallet" class="col-lg-2 control-label">Wallet Address</label>
        <input id="wallet" type="text" value={this.state.wallet} disabled/>

        <label for="fullName" class="col-lg-2 control-label">First Name - Last Name</label>
        <input id="fullName" type="text" value={this.state.fullName} disabled/>

        <label for="department" class="col-lg-2 control-label">Department</label>
        <input id="department" type="text" value={this.state.department} disabled/>

        <label for="faculty" class="col-lg-2 control-label">Faculty</label>
        <input id="faculty" type="text" value={this.state.faculty} disabled/>

        <hr />

        {this.renderGradeFields(this.state.subjects)}

        <button id="button" onClick={this.onClick}>Submit Grade</button>
        {this.state.success && (<p>Submit successfully! If you want to check student info, please click this <a href={"https://rinkeby.etherscan.io/tx/"+this.state.transactionHash} target="_blank">link</a></p>)}
        {this.state.success && (<p>(TxHash : {this.state.transactionHash})</p>)}
      </div>

    );
  }
}

export default Admin;
