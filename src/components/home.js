import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

class Home extends Component {
  constructor(props){
    super(props)
  }

  render() {
    return (
      <div className="Home">
        <p><Link to="/login">Admin</Link></p>
        <p><Link to="/grade">Student</Link></p>
      </div>

    );
  }
}

export default Home;
