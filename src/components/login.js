import React, { Component } from 'react';

import '../App.css';

class Login extends Component {
  constructor(props){
    super(props)
    this.state = {
      username: '',
      password: '',
    }
  }

  handleChangeUsername = (e) => {
    this.setState({username: e.target.value})
  }

  handleChangePassword = (e) => {
    this.setState({password: e.target.value})
  }

  onSubmit = (e) => {
    e.preventDefault()

    if(this.state.username==='admin' && this.state.password==='password'){
      return this.props.history.push('/admin')
    }
    return alert('Worng username or password. Please try again.')
  }

  render() {
    return (
      <div className="Login">
        <form id="login-form" onSubmit={this.onSubmit}>
          <div class="form-group">
              <label for="username">Username</label>
              <input type="username" onChange={this.handleChangeUsername} class="form-control" id="username" aria-describedby="usernameHelp" placeholder="Enter Admin Username" />
              {/* <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> */}
          </div>
          <div class="form-group">
              <label for="password">Password</label>
              <input type="password" onChange={this.handleChangePassword} class="form-control" id="password" placeholder="Password" />
          </div>
          {/* <div class="form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Check me out</label>
          </div> */}
          <button type="submit" class="btn btn-dark">Submit</button>
        </form>
      </div>

    );
  }
}

export default Login;
