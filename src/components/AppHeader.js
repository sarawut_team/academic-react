import React, { Component } from 'react';
import logo from '../LogoIndex.png';
import '../App.css';

class AppHeader extends Component {
  
  render() {
    return (
      <div className="Header">
        <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">Welcome to Academic Smart Contract Grade Report</h1>
        </header>
        <h1>Academic Grade Report</h1>
        <p className="App-intro">
          This site create to complete Master Project and using Blockchain technology.
        </p>
      </div>
    );
  }
}

export default AppHeader;
