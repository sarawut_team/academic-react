import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import './App.css';
import AppHeader from './components/AppHeader'

import Home from './components/home';
import Login from './components/login';
import Admin from './components/admin';
import Grade from './components/grade';

class App extends Component {
  
  render(props) {
    return (
      <div className="App">
        <AppHeader/>

        <div className="Container">
          <br/>
          <Route exact path='/' component={Home} />
          <Route path='/login' component={Login} />
          <Route path='/admin' component={Admin} />
          <Route path='/grade' component={Grade} />
        </div>
      </div>
    );
  }
}

export default App;
